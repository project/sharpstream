((Drupal, once) => {
  Drupal.behaviors.sharpstreamAudio = {
    attach(context) {
      const audioPlayers = once(
        'sharpstreamAudio',
        '.sharpstream-audio__player',
        context,
      );

      if (!audioPlayers.length) {
        return true;
      }

      // Loops every audio player and turns MP3 file into an interactive audio player with styles.
      audioPlayers.forEach((audioPlayer) => {
        const uploadedAudio = audioPlayer
          .querySelector('audio source')
          .getAttribute('src');
        const audio = new Audio(uploadedAudio);
        const playBtn = audioPlayer.querySelector('.controls .toggle-play');
        const timeline = audioPlayer.querySelector('.timeline');
        const volumeEl = audioPlayer.querySelector('.volume-button');

        /**
         * Turns 128 second duration to print into 2:08.
         * @param {number} num is the audio duration.
         * @return {number} which is the converted duration of the audio.
         */
        function getTimeCodeFromNum(num) {
          let seconds = parseInt(num, 10);
          let minutes = parseInt(seconds / 60, 10);
          seconds -= minutes * 60;
          const hours = parseInt(minutes / 60, 10);
          minutes -= hours * 60;

          if (hours === 0) {
            return `${minutes}:${String(seconds % 60).padStart(2, 0)}`;
          }
          return `${String(hours).padStart(2, 0)}:${minutes}:${String(
            seconds % 60,
          ).padStart(2, 0)}`;
        }

        audio.addEventListener('loadeddata', () => {
          audioPlayer.querySelector('.time .length').textContent =
            getTimeCodeFromNum(audio.duration);
          audio.volume = 0.75;
        });

        // Click on timeline to skip around.
        timeline.addEventListener('click', (e) => {
          const timelineWidth = window.getComputedStyle(timeline).width;
          audio.currentTime =
            (e.offsetX / parseInt(timelineWidth, 10)) * audio.duration;
        });

        // Check audio percentage and update time accordingly.
        setInterval(() => {
          const progressBar = audioPlayer.querySelector('.progress');
          const width = (audio.currentTime / audio.duration) * 100;
          progressBar.style.width = `${width}%`;
          audioPlayer.querySelector('.time .current').textContent =
            getTimeCodeFromNum(audio.currentTime);
        }, 500);

        // Toggle between playing and pausing on button click.
        playBtn.addEventListener('click', () => {
          if (audio.paused && playBtn.classList.contains('play')) {
            playBtn.classList.remove('play');
            playBtn.classList.add('pause');
            audio.play();
          } else if (playBtn.classList.contains('pause')) {
            playBtn.classList.remove('pause');
            playBtn.classList.add('play');
            audio.pause();
          }
        });

        // Mute and unmute.
        volumeEl.addEventListener('click', () => {
          audio.muted = !audio.muted;
          if (audio.muted) {
            volumeEl.classList.remove('full-volume');
            volumeEl.classList.add('mute');
          } else {
            volumeEl.classList.add('full-volume');
            volumeEl.classList.remove('mute');
          }
        });
      });
    },
  };
})(Drupal, once);
