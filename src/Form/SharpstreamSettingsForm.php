<?php

namespace Drupal\sharpstream\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a settings form for the sharpstream module.
 */
class SharpstreamSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sharpstream.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sharpstream_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sharpstream.settings');

    // Public directory for sharpstream files.
    $form['public_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Directory'),
      '#description' => $this->t('The directory where Sharpstream files are stored.'),
      '#default_value' => $config->get('public_directory'),
    ];

    // Sharpstream API Url.
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#description' => $this->t('The URL of the Sharpstream API.'),
      '#default_value' => $config->get('api_url'),
    ];

    // Sharpstream Bearer token.
    $form['bearer_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bearer Token'),
      '#description' => $this->t('The Bearer token for the Sharpstream API.'),
      '#default_value' => $config->get('bearer_token'),
      '#maxlength' => 1000,
    ];

    // List of Sharpstream Customer IDs.
    $form['customer_ids'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Customer IDs'),
      '#description' => $this->t('List of Sharpstream Customer IDs. One per line.'),
      '#default_value' => $config->get('customer_ids'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sharpstream.settings')
      ->set('public_directory', $form_state->getValue('public_directory'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('bearer_token', $form_state->getValue('bearer_token'))
      ->set('customer_ids', $form_state->getValue('customer_ids'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
