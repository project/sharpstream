<?php

namespace Drupal\sharpstream;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides common interface for Sharpstream entities.
 */
interface SharpstreamEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Sharpstream CMS entity name.
   *
   * @return string
   *   Name of the Sharpstream CMS entity.
   */
  public function getName();

  /**
   * Sets the Sharpstream CMS entity name.
   *
   * @param string $name
   *   The Sharpstream CMS entity name.
   */
  public function setName($name);

  /**
   * Returns the description.
   *
   * @return string
   *   The description of the CMS entity.
   */
  public function getDescription();

  /**
   * Sets the CMS entity's description.
   *
   * @param string $description
   *   The description of the CMS entity.
   */
  public function setDescription($description);

  /**
   * Gets the Sharpstream CMS entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Sharpstream CMS entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Sharpstream CMS entity creation timestamp.
   *
   * @param int $timestamp
   *   The Sharpstream CMS entity creation timestamp.
   */
  public function setCreatedTime($timestamp);

}
