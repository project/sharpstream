<?php

namespace Drupal\sharpstream\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Sharpstream Audio.
 *
 * @see \Drupal\sharpstream\Entity\SharpstreamAudio
 */
class SharpstreamAudioAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\sharpstream\Entity\SharpstreamAudio $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          /* cspell:disable-next-line */
          return AccessResult::allowedIfHasPermission($account, 'view unpublished sharpstream_audio entities');
        }
        /* cspell:disable-next-line */
        return AccessResult::allowedIfHasPermission($account, 'view published sharpstream_audio entities');

      case 'update':
        /* cspell:disable-next-line */
        return AccessResult::allowedIfHasPermission($account, 'edit sharpstream_audio entities');

      case 'delete':
        /* cspell:disable-next-line */
        return AccessResult::allowedIfHasPermission($account, 'delete sharpstream_audio entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    /* cspell:disable-next-line */
    return AccessResult::allowedIfHasPermission($account, 'add sharpstream_audio entities');
  }

}
