<?php

namespace Drupal\sharpstream\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;
use Drupal\sharpstream\SharpstreamEntityInterface;
use Drupal\time_formatter\Plugin\Field\FieldFormatter\TimeFieldFormatter;
use Drupal\user\UserInterface;

/**
 * Defines the Sharpstream Audio entity.
 *
 * @ingroup sharpstream
 *
 * @ContentEntityType(
 *   id = "sharpstream_audio",
 *   label = @Translation("Sharpstream Audio"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\sharpstream\SharpstreamAudioListBuilder",
 *     "views_data" = "Drupal\sharpstream\Entity\SharpstreamAudioViewsData",
 *     "access" = "Drupal\sharpstream\Access\SharpstreamAudioAccessControlHandler",
 *   },
 *   base_table = "sharpstream_audio",
 *   admin_permission = "access sharpstream audio overview page",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/content/sharpstream_audio",
 *   },
 * )
 */
class SharpstreamAudio extends ContentEntityBase implements SharpstreamEntityInterface {

  use EntityChangedTrait;

  /**
   * Sharpstream audio tags vocabulary ID.
   */
  const SHARPSTREAM_TAGS_VID = 'sharpstream_audio_tags';

  /**
   * SharpstreamAudio images path.
   */
  const SHARPSTREAM_IMAGES_DIR = 'sharpstream/images';

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // Set weights based on the real order of the fields.
    $weight = -30;

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Sharpstream Audio entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The Drupal UUID.'))
      ->setReadOnly(TRUE);

    // Status field, tied together with the status of the entity.
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDescription(t('Determines whether the audio is playable.'))
      ->setDefaultValue(TRUE)
      ->setSettings([
        'on_label' => t('Published'),
        'off_label' => t('Unpublished'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'label' => 'above',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'inline',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Title of the audio.'))
      ->setRequired(TRUE)
      ->setSettings([
        // Not applying the max_length setting any longer. Without an explicit
        // max_length setting Drupal will use a varchar(255) field, at least on
        // my MySQL backend. BC docs currently say the length of the 'name'
        // field is 1..255, but let's just not apply any explicit limit any
        // longer on the Drupal end.
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'hidden',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Sharpstream Audio description.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tags'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tags'))
      ->setDescription(t('Max 1200 tags per audio'))
      // We can't really say 1200 here as it'd yield 1200 textfields on the UI.
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings([
        'target_type' => 'taxonomy_term',
        'handler_settings' => [
          'target_bundles' => [self::SHARPSTREAM_TAGS_VID => self::SHARPSTREAM_TAGS_VID],
          'auto_create' => TRUE,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => ++$weight,
        'settings' => [
          'autocomplete_type' => 'tags',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['duration'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Audio Duration (s)'))
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number_time',
        'label' => 'inline',
        'weight' => ++$weight,
        'settings' => [
          // @phpstan-ignore-next-line
          'storage' => TimeFieldFormatter::STORAGE_SECONDS,
          // @phpstan-ignore-next-line
          'display' => TimeFieldFormatter::DISPLAY_NUMBERS,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['audio_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Audio ID'))
      ->setDescription(t('Unique Audio ID assigned by Sharpstream.'))
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'inline',
        'weight' => ++$weight,
      ])
      ->setDisplayConfigurable('view', TRUE);

    // Provide an external URL.
    $fields['audio_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Audio storage file URL'))
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'uri_link',
        'label' => 'inline',
        'weight' => $weight,
        'settings' => [
          'trim_length' => 500,
          'target' => '_blank',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Provide an external URL.
    $fields['audio_json_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Audio waveform json URL'))
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'uri_link',
        'label' => 'inline',
        'weight' => $weight,
        'settings' => [
          'trim_length' => 500,
          'target' => '_blank',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Sharpstream audio artwork'))
      ->setSettings([
        'file_extensions' => 'jpg jpeg png',
        'file_directory' => self::SHARPSTREAM_IMAGES_DIR,
        'alt_field' => FALSE,
        'alt_field_required' => FALSE,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => ++$weight,
      ])
      ->setDisplayOptions('view', [
        'type' => 'image',
        'label' => 'above',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the Sharpstream Audio was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => ++$weight,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Sharpstream Audio was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Creates or updates a Sharpstream audio entity.
   *
   * @param object $api_audio
   *   A Sharpstream audio item.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   Sharpstream audio storage.
   * @param bool $force_save
   *   Whether to force save the item.
   *
   * @return \Drupal\sharpstream\Entity\SharpstreamAudio|null
   *   The saved Sharpstream entity.
   */
  public static function createOrUpdate($api_audio, EntityStorageInterface $storage, $force_save = FALSE) {
    $needs_save = FALSE;

    // Try to get an existing audio.
    $existing_audio = $storage->getQuery()
      ->condition('audio_id', $api_audio->uuid)
      ->accessCheck(FALSE)
      ->execute();

    // Update existing audio.
    if (!empty($existing_audio)) {
      // Load Sharpstream Audio.
      /** @var SharpstreamAudio $audio_entity */
      $audio_entity = self::load(reset($existing_audio));

      // Update audio if it is changed on Sharpstream.
      if ($audio_entity->getChangedTime() < strtotime($api_audio->updated_at)) {
        $needs_save = TRUE;
      }
    }
    // Create audio if it does not exist.
    else {
      // Create new Sharpstream Audio entity.
      $values = [
        'uid' => 1,
        'audio_id' => $api_audio->uuid,
        'created' => strtotime($api_audio->created_at),
      ];
      $audio_entity = self::create($values);
      $needs_save = TRUE;
    }

    // Save entity only if it is being created or updated.
    if ($needs_save || $force_save) {
      // Save or update changed time.
      $audio_entity->setChangedTime(strtotime($api_audio->updated_at));

      // Save or update Name field if needed.
      if ($audio_entity->getName() != ($name = $api_audio->name)) {
        $audio_entity->setName($name);
      }

      // Save or update Status field if needed.
      if ($audio_entity->isPublished() != ($published = $api_audio->published)) {
        $audio_entity->setPublished($published);
      }

      // Save or update Description field if needed.
      if ($audio_entity->getDescription() != ($description = $api_audio->description)) {
        $audio_entity->setDescription($description);
      }

      // Save or update Duration field if needed.
      if ($audio_entity->getDuration() != ($duration = $api_audio->storage->ffmpeg_details->duration)) {
        $audio_entity->setDuration($duration);
      }

      // Save or update Audio URL field if needed.
      if ($audio_entity->getAudioUrl() != ($audio_url = $api_audio->storage_file_url)) {
        $audio_entity->setAudioUrl($audio_url);
      }

      // Save or update Audio URL field if needed.
      if ($audio_entity->getAudioJsonUrl() != ($audio_json_url = $api_audio->waveform_json_url)) {
        $audio_entity->setAudioJsonUrl($audio_json_url);
      }

      // Save or update poster image if needed.
      if (!empty($api_audio->artwork)) {
        $audio_entity->saveImage($api_audio->artwork->path, $force_save);
      }
      // Otherwise leave empty or remove poster from SharpstreamAudio entity.
      else {
        // Delete file.
        $audio_entity->setImage(NULL);
      }

      $audio_entity->save();
    }

    return $audio_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDuration() {
    return $this->get('duration')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDuration($duration) {
    $this->set('duration', $duration);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAudioId() {
    return $this->get('audio_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAudioId($id) {
    $this->set('audio_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    return $this->set('status', $published ? NodeInterface::PUBLISHED : NodeInterface::NOT_PUBLISHED);
  }

  /**
   * {@inheritdoc}
   */
  public function getAudioUrl() {
    return $this->get('audio_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAudioUrl($audio_url) {
    $this->set('audio_url', $audio_url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAudioJsonUrl() {
    return $this->get('audio_json_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAudioJsonUrl($audio_url) {
    $this->set('audio_json_url', $audio_url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getImage() {
    $values = $this->get('image')->getValue();

    if (empty($values[0]['target_id'])) {
      return NULL;
    }

    $this->provideDefaultValuesForImageField($values);

    return $values[0];
  }

  /**
   * {@inheritdoc}
   */
  public function setImage($image) {
    // Handle image deletion here as well.
    $image_to_delete = $this->getImage();
    if (is_null($image) && !empty($image_to_delete['target_id'])) {
      $this->deleteFile($image_to_delete['target_id']);
    }

    $this->set('image', $image);
    return $this;
  }

  /**
   * Helper function to delete a file from the entity.
   *
   * @param int $target_id
   *   The target ID of the saved file.
   *
   * @throws \Exception
   *   If the $target_id is not a positive number or zero.
   */
  private function deleteFile($target_id) {
    if (!is_numeric($target_id) || $target_id <= 0) {
      throw new \Exception('Target ID must be non-zero number.');
    }

    /** @var \Drupal\file\Entity\File $file */
    $file = File::load($target_id);

    // Delete image.
    if (!is_null($file)) {
      $file->delete();
    }
  }

  /**
   * Helper function to provide default values for image fields.
   *
   * The original and the save entity's field arrays is a bit different from
   * each other, so provide the missing values.
   *
   * @param array &$values
   *   The field's values array.
   */
  protected function provideDefaultValuesForImageField(array &$values) {
    /** @var \Drupal\file\Entity\File $file */
    foreach ($values as &$value) {
      $file = File::load($value['target_id']);

      if (!is_null($file)) {
        $value += [
          'display' => $file->status->value,
          'description' => '',
          'upload' => '',
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function saveImage($image, $force_save = NULL) {
    // Prepare function name and throw an exception if it doesn't match for an
    // existing function.
    $image_dir = 'public://' . self::SHARPSTREAM_IMAGES_DIR;

    $needs_save = TRUE;

    // Try to get the image's filename from Sharpstream.
    preg_match('/\/(?!.*\/)([\w\-\.]+)/i', $image, $matches);
    if (!isset($matches[1])) {
      return $this;
    }
    $filename = $matches[1];

    if (!empty($filename)) {
      // Get entity's image file.
      /** @var \Drupal\file\Entity\File $file */
      $entity_image = $this->getImage();
      // If the entity already has an image then load and delete it.
      if (!empty($entity_image['target_id'])) {
        $file = File::load($entity_image['target_id']);
        if (!is_null($file) && $file->getFileName() != $filename) {
          $this->setImage(NULL);
        }
        elseif (!is_null($file) && $file->getFilename() == $filename) {
          $needs_save = FALSE;
        }
      }

      // Save the new image from Sharpstream to the entity.
      if ($needs_save || $force_save) {
        $image_content = file_get_contents($image);
        // Prepare directory and if it was a success try to save the image.
        if (\Drupal::service('file_system')->prepareDirectory($image_dir, FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY)) {
          $file = \Drupal::service('file.repository')->writeData($image_content, "{$image_dir}/{$filename}");

          // Set image if there was no error.
          if ($file !== FALSE) {
            $this->setImage($file->id());
          }
        }
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

}
