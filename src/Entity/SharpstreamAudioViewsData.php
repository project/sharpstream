<?php

namespace Drupal\sharpstream\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data.
 */
class SharpstreamAudioViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['sharpstream_audio']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Sharpstream Audio'),
      'help' => $this->t('The Sharpstream Audio ID.'),
    ];

    return $data;
  }

}
