<?php

namespace Drupal\sharpstream;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\sharpstream\Entity\SharpstreamAudio;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Utils;

/**
 * Sharpstream API service.
 */
class SharpstreamService {

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * FileRepository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * HttpClient.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /* cspell:disable */
  /**
   * The Sharpstream ntity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;
  /* cspell:enable */

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Construct Sharpstream object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, FileRepositoryInterface $file_repository, ClientInterface $http_client, AccountInterface $current_user, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->httpClient = $http_client;
    $this->currentUser = $current_user;
    $this->storage = $entity_type_manager->getStorage('sharpstream_audio');
    $this->loggerFactory = $logger_factory->get('sharpstream');
    $this->config = $config_factory->get('sharpstream.settings');
  }

  /**
   * Call Sharpstream API and process results.
   *
   * @return void
   *   Void.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function process(string $active_customer = '') {
    // Set timestamp.
    // $flag = current time.
    $results = $this->getSharpstreamApi('', $active_customer);

    $directory = $this->config->get('public_directory');
    $file_system = $this->fileSystem;
    $file_system->prepareDirectory($directory, $file_system::CREATE_DIRECTORY);

    $total = $results->paginator->total;
    $pages = $results->paginator->total_pages;
    $page = 1;
    $replacements = [
      '%total' => $total,
      '%pages' => $pages,
    ];

    $this->loggerFactory->info('Total number of audio items: %total', $replacements);
    $this->loggerFactory->info('Total number of API pages: %pages', $replacements);

    while ($page <= $pages) {
      $this->loggerFactory->info('...Processing API page #%page', ['%page' => $page]);
      $results = $this->getSharpstreamApi($page, $active_customer);

      foreach ($results->data as $result) {
        /** @var \Drupal\sharpstream\Entity\SharpstreamAudio $audio_entity */
        $audio_entity = SharpstreamAudio::createOrUpdate($result, $this->storage, FALSE);

        $result_replacements = [
          '%result_id' => $result->uuid,
          '%audio_id' => $audio_entity->id(),
        ];

        $this->loggerFactory->info('......Synced Sharpstream ID %result_id into drupal entity %audio_id', $result_replacements);
      }

      $page++;
    }

    // When complete, set $flag into state for the next run. This should
    // provide a short overlap in timestamps, so that we never miss items.
  }

  /**
   * Function to call the Sharpstream API and return paginated data.
   *
   * @param int $page
   *   The page number.
   * @param string $active_customer
   *   The active customer.
   *
   * @return mixed
   *   The decoded api response json.
   */
  private function getSharpstreamApi($page = '', string $active_customer = '') {
    if (empty($active_customer)) {
      $uri = $this->config->get('api_url') . '?limit=100&page=' . $page;
    }
    else {
      $uri = $this->config->get('api_url') . '?limit=100&active_customer=' . $active_customer . '&page=' . $page;
    }
    $response = $this->httpClient->get($uri, [
      'headers' => [
        'Authorization' => $this->config->get('bearer_token'),
        'Accept' => 'application/json',
      ],
    ]);

    $response_body = $response->getBody()->getContents();
    return Utils::jsonDecode($response_body);
  }

}
