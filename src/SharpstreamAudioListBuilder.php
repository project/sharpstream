<?php

namespace Drupal\sharpstream;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Sharpstream Audio.
 *
 * @ingroup sharpstream
 */
class SharpstreamAudioListBuilder extends EntityListBuilder {

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $accountProxy;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   Entity storage.
   * @param \Drupal\Core\Session\AccountProxy $account_proxy
   *   Account proxy.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   Date formatter.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, AccountProxy $account_proxy, DateFormatter $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->accountProxy = $account_proxy;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_user'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(FALSE)
      ->sort('changed', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // Assemble header columns.
    $header = [
      'audio' => $this->t('Audio'),
      'audio_id' => $this->t('Audio ID'),
      'name' => $this->t('Name'),
      'status' => $this->t('Status'),
      'audio_url' => $this->t('Audio URL'),
      'created' => $this->t('Created'),
      'updated' => $this->t('Updated'),
    ];

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $name = $entity->getName();
    $audio_id = $entity->getAudioId();
    $audio_url = $entity->getAudioUrl();

    // Get thumbnail image style and render it.
    $image = $entity->getImage();

    $thumbnail_image = '';
    if (!empty($image['target_id'])) {
      /** @var \Drupal\file\Entity\File $thumbnail_file */
      $thumbnail_file = File::load($image['target_id']);
      /** @var \Drupal\image\Entity\ImageStyle $image_style */
      $image_style = ImageStyle::load('thumbnail');
      if (!empty($thumbnail_file) && !is_null($image_style)) {
        $image_uri = $image_style->buildUrl($thumbnail_file->getFileUri());
        $thumbnail_image = "<img src='{$image_uri}' alt='{$entity->getName()}'>";
      }
    }

    // Assemble row.
    $row = [
      'audio' => [
        'data' => [
          '#markup' => $thumbnail_image,
        ],
      ],
      'audio_id' => $audio_id,
      'name' => $name,
      'status' => $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
      'audio_url' => $audio_url,
      'created' => $this->dateFormatter->format($entity->getCreatedTime(), 'short'),
      'updated' => $this->dateFormatter->format($entity->getChangedTime(), 'short'),
    ];

    return $row;
  }

}
