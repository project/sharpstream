<?php

namespace Drupal\sharpstream\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\sharpstream\SharpstreamService;
use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\sharpstream\Commands
 */
class SharpstreamCommands extends DrushCommands {

  /**
   * The Sharpstream service.
   *
   * @var \Drupal\sharpstream\SharpstreamService
   */
  protected SharpstreamService $sharpstream;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Construct SharpstreamCommands object.
   *
   * @param \Drupal\sharpstream\SharpstreamService $sharpstream
   *   The Sharpstream service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration object factory.
   */
  public function __construct(SharpstreamService $sharpstream, ConfigFactoryInterface $configFactory) {
    parent::__construct();
    $this->sharpstream = $sharpstream;
    $this->configFactory = $configFactory;
  }

  /**
   * Drush command for calling the Sharpstream API service.
   *
   * @command sharpstream
   *
   * @aliases sharpstream
   *
   * @usage sharpstream
   */
  public function processSharpstream() {
    $config = $this->configFactory->get('sharpstream.settings');
    $customer_ids = $config->get('customer_ids');
    $customer_ids = explode("\n", str_replace("\r", "", $customer_ids));

    foreach ($customer_ids as $customer_id) {
      $this->sharpstream->process($customer_id);
    }
  }

}
